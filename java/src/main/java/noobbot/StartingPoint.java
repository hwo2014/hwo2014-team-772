package noobbot;

public class StartingPoint {
	Position position;
	float angle;

	public StartingPoint(Position position, String angle) {
		this.position = Main.gson.fromJson(position.toString(), Position.class);
		this.angle = Float.parseFloat(angle);
	}

	@Override
		public String toString() {
			return position.toString() + ", angle=" + angle;
		}

	private class Position{
	 int x;
	 int y;
	
	 @SuppressWarnings("unused")
	public Position(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public String toString() {
		return "[x=" + x + ", y=" + y + "]";
	}

	}
}
