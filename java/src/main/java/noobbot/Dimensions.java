package noobbot;

public class Dimensions {
	float length;
	float width;
	float guideFlagPosition;
	
	public Dimensions(float length, float width, float guideFlagPosition) {
		this.length = length;
		this.width = width;
		this.guideFlagPosition = guideFlagPosition;
	}

	@Override
	public String toString() {
		return "Dimensions [length=" + length + ", width=" + width
				+ ", guideFlagPosition=" + guideFlagPosition + "]";
	}
	
	
}
