package noobbot;

public class GameInit {
	Race race = null;

	public GameInit(Object data) {
		this.race = Main.gson.fromJson(data.toString(), Race.class);
	}

	@Override
	public String toString() {
		String message;
		message = "Printing GameInit:" + race.toString();
		return message;
	}
}
