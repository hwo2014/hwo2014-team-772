package noobbot;

public class RaceSession {
 int laps;
 int maxLapTimeMs;
 boolean quickRace;

 public RaceSession(int laps, int maxLapTimeMs, boolean quickRace) {
	this.laps = laps;
	this.maxLapTimeMs = maxLapTimeMs;
	this.quickRace = quickRace;
}

@Override
public String toString() {
	return "RaceSession [laps=" + laps + ", maxLapTimeMs=" + maxLapTimeMs
			+ ", quickRace=" + quickRace + "]";
}
}
