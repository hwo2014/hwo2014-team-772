package noobbot;

public class ID {
	String name; 
	String color;
	
	public ID(String name, String color) {
		super();
		this.name = name;
		this.color = color;
	}

	@Override
	public String toString() {
		return "ID [name=" + name + ", color=" + color + "] ";
	}
	
}
