package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.gson.Gson;

public class Main {

    public static Car myCar;
    public static GameInit game;
	
    public static void main(String[] args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];
        String trackName = "germany";

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey + " " +trackName);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey, trackName, new BotId(botName, botKey)));
    }

    static final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;
        //String line = "{\"msgType\":\"gameInit\",\"data\":{\"race\":{\"track\":{\"id\":\"keimola\",\"name\":\"Keimola\",\"pieces\":[{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":200,\"angle\":22.5,\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"radius\":200,\"angle\":-22.5},{\"length\":100.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":-45.0},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":200,\"angle\":22.5},{\"radius\":200,\"angle\":-22.5},{\"length\":100.0,\"switch\":true},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"length\":62.0},{\"radius\":100,\"angle\":-45.0,\"switch\":true},{\"radius\":100,\"angle\":-45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"radius\":100,\"angle\":45.0},{\"length\":100.0,\"switch\":true},{\"length\":100.0},{\"length\":100.0},{\"length\":100.0},{\"length\":90.0}],\"lanes\":[{\"distanceFromCenter\":-10,\"index\":0},{\"distanceFromCenter\":10,\"index\":1}],\"startingPoint\":{\"position\":{\"x\":-300.0,\"y\":-44.0},\"angle\":90.0}},\"cars\":[{\"id\":{\"name\":\"\'alexkiilla\'\",\"color\":\"red\"},\"dimensions\":{\"length\":40.0,\"width\":20.0,\"guideFlagPosition\":10.0}},{\"id\":{\"name\":\"\'alexkiilla\'\",\"color\":\"red\"},\"dimensions\":{\"length\":40.0,\"width\":20.0,\"guideFlagPosition\":10.0}}],\"raceSession\":{\"laps\":3,\"maxLapTimeMs\":60000,\"quickRace\":true}}},\"gameId\":\"0762f9bc-fc0d-42b0-b021-cfb7d1e1edc7\"}";
        //String line = "{\"msgType\": \"yourCar\", \"data\": {\"name\": \"Schumacher\",\"color\": \"red\"}}";
        send(join);
        
        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("yourCar")) {
                //myCar = gson.fromJson(msgFromServer.data.toString(), Car.class);
                //System.out.println(myCar.toString());
            }else if (msgFromServer.msgType.equals("carPositions")) {
            	PiecePosition prevPosition = myCar.piecePosition;
            	
            	Car[] cp = gson.fromJson(msgFromServer.data.toString(), Car[].class);
            
        		StringBuilder sb = new StringBuilder("\nCar:");
        		for(Car c : cp){
        			myCar = c;
        			myCar.prevPosition = prevPosition;
        			sb.append("\n\t");
        			//sb.append(c.toString());
        		}
        		//System.out.println(sb.toString());
                send(TaoPaiPai.MakeImportantDecisions());
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                System.out.println("Race init");
                game = gson.fromJson(msgFromServer.data.toString(), GameInit.class);
                System.out.println(game.toString());
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else {
                send(new Ping());
            }
        }
   }

    static double getSpeed(){
        double currSpeed = 1;
        try{
	        if(myCar.lane.endLaneIndex == 1){
		    	if(myCar.piecePosition.pieceIndex + 1 != 40){
			    	if(Math.abs(myCar.angle) > 40)
			    		currSpeed = 0.05;
			    	if(game.race.track.pieces.get(myCar.piecePosition.pieceIndex + 1).radius > 20.0 && myCar.getSpeed() > 400)
			    		currSpeed = 0.01;
			    	else 
			    		currSpeed = 1;
		    	}else{
		    		currSpeed = 1;
		    	}
	    	}else{
		    	if(myCar.piecePosition.pieceIndex + 1 != 40){
			    	if(Math.abs(myCar.angle) > 30)
			    		currSpeed = 0.05;
			    	if(game.race.track.pieces.get(myCar.piecePosition.pieceIndex + 1).radius > 20.0 && myCar.getSpeed() > 400)
			    		currSpeed = 0.01;
			    	else 
			    		currSpeed = 1;
		    	}else{
		    		currSpeed = 1;
		    	}
	    	}
        }catch(NullPointerException ex){}
    	/*if(myCar.CarSpeed() < 400 && currSpeed < 1 && Math.abs(myCar.angle) < 40){
    		currSpeed += .1;
    	}
    	if(Math.abs(myCar.angle) > 40 && myCar.CarSpeed() > 400){
    		currSpeed = .03;
    	}*/
    	return currSpeed;
    }
    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final BotId botId;
    public final String trackName;
    public final String password;
    public final int carCount;

    Join(final String name, final String key, final String trackName, final BotId  botId) {
        this.botId = botId;
        this.trackName = trackName;
        this.password = "perras";
        this.carCount = 3;
    }

    @Override
    protected String msgType() {
        return "createRace";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class SwitchLane extends SendMsg {
    private String value;

    public SwitchLane(String value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}

class BotId{
	String name;
	String key;
	
	public BotId(final String name, final String key) {
		this.name = name;
		this.key = key;
	}
}