package noobbot;

public class Car {
	ID id = null;
	Dimensions dimensions = null;
	PiecePosition piecePosition;
	PiecePosition prevPosition;
	Lane lane;
	float angle;

	public Car(ID id, Dimensions dimensions, float angle,
			PiecePosition piecePosition, int lap) {
		this.id = id;
		this.dimensions = dimensions;
		this.angle = angle;
		this.piecePosition = Main.gson.fromJson(piecePosition.toString(),
				PiecePosition.class);
	}

	@Override
	public String toString() {
		return "Car [id=" + id + ", dimensions=" + dimensions + ", angle="
				+ angle + "] \n Car Speed = " + getSpeed() + " Awesomes/h";
	}

	public double getSpeed(){
		//System.out.println("Lenght: "+ Main.game.race.track.pieces.get(piecePosition.pieceIndex).getLenght());
		//System.out.println("InpieceD: "+piecePosition.inPieceDistance);
		try {
			if(piecePosition.pieceIndex == prevPosition.pieceIndex){
				return (piecePosition.inPieceDistance - prevPosition.inPieceDistance) / ((float)1/60);
			}else
			{
				float x = Main.game.race.track.pieces.get(prevPosition.pieceIndex).length - prevPosition.inPieceDistance + piecePosition.inPieceDistance;
				return x / (1 / 60);
			}
		} catch (NullPointerException e) {
			//System.out.println("nullpointerPrinting0");
			return 0.0f;
		}
	}
}
