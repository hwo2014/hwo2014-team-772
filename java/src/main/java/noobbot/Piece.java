package noobbot;

import com.google.gson.annotations.SerializedName;

public class Piece {
	float length;
	@SerializedName("switch") boolean switche;
	float radius;
	float angle;
	
	public Piece(float length, boolean switche, float radius, float angle) {
		super();
		this.length = length;
		this.switche = switche;
		this.radius = radius;
		this.angle = angle;
	}

	float getLenght(){
		if(this.angle > 0){
			float rad = this.angle * 3.1416f / 200;
			this.length = rad * radius;
		}
		return this.length;
	}
	
	@Override
	public String toString() {
		return "Piece [length=" + length + ", switch=" + switche + ", radius="
				+ radius + ", angle=" + angle + "]";
	}
}
