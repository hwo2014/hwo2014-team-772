package noobbot;

import java.util.List;

public class Race {
	
	Track track = null;
	List<Car> cars = null;
	RaceSession raceSession = null;

	@SuppressWarnings("unchecked")
	public Race(Track track, List<Car> cars, RaceSession raceSession) {
		this.track = Main.gson.fromJson(track.toString(), Track.class);
		this.cars = Main.gson.fromJson(cars.toString(), List.class);
		this.raceSession = Main.gson.fromJson(raceSession.toString(), RaceSession.class);
	}
	
	@SuppressWarnings("unchecked")
	public Race(List<Car> cars) {
		this.cars = Main.gson.fromJson(cars.toString(), List.class);
	}
	
	public String PrintCars(){
		StringBuilder sb = new StringBuilder("\nCars:");
		for(Car c : cars){
			Main.myCar = c;
			sb.append("\n\t");
			sb.append("[id=" + c.id + "dimensions=" + c.dimensions +"]");
		}
		return sb.toString();
	}

	@Override
	public String toString() {
		return "\nRace Data: " + "\n" + track.toString() + PrintCars() +"\n"+ raceSession.toString();
	}
}