package noobbot;

import java.util.List;

public class Track {
	String id;
	String name;
	List<Piece> pieces;
	List<Lane> lanes;
	StartingPoint startingPoint; 
	
	@SuppressWarnings("unchecked")
	public Track(String id, String name, List<Piece> pieces, List<Lane> lanes,
			StartingPoint startingPoint) {
		this.id = id;
		this.name = name;
		this.pieces = Main.gson.fromJson(pieces.toString(), List.class);
		this.lanes = Main.gson.fromJson(lanes.toString(), List.class);
		this.startingPoint = Main.gson.fromJson(startingPoint.toString(), StartingPoint.class);
	}

	public String PrintPieces(){
		StringBuilder sb = new StringBuilder("\n{Pieces:");
		for(Piece p : pieces){
			sb.append("\n\t");
			sb.append(p.toString());
		}
		sb.append("}");
		return sb.toString();
	}

	public String PrintLanes(){
		StringBuilder sb = new StringBuilder("\n{Lanes:");
		for(Lane l : lanes){
			sb.append("\n\t");
			sb.append(l.toString());
		}
		sb.append("}");
		return sb.toString();
	}

	@Override
	public String toString() {
		return "Track [id=" + id + ", name=" + name + ", " + PrintPieces()
				+ ", " + PrintLanes() + ", \nstartingPoint=" + startingPoint.toString() + "]";
	}
	
	
}
