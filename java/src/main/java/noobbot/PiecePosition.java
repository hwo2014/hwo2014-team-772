package noobbot;

public class PiecePosition {
	int pieceIndex;
	float inPieceDistance;
	int lap;
	
	public PiecePosition(int pieceIndex, float inPieceDistance, int lap){
		this.pieceIndex = pieceIndex;
		this.inPieceDistance = inPieceDistance;
		this.lap = lap;
	}

	@Override
	public String toString(){
		
		return "Piece index: " + pieceIndex + " In piece distance: " + inPieceDistance + " Lap: " + lap;
	}

}
