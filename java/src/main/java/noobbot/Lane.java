package noobbot;

public class Lane {
	int distanceFromCenter;
	int index;
    int startLaneIndex;
    int endLaneIndex;
	
	public Lane(int distanceFromCenter, int index) {
		this.distanceFromCenter = distanceFromCenter;
		this.index = index;
	}

	@Override
	public String toString() {
		return "Lane [distanceFromCenter=" + distanceFromCenter + ", index="
				+ index + "]";
	}
}
